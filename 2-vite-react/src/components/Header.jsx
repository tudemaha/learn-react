export default function Header({ name }) {
	return <h1>{name ? name : "Tude"} Lagi Belajar React 🚀</h1>;
}
