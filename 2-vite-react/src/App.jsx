import { useState } from "react";
import Header from "./components/Header";

const students = ["Tude", "Sahur", "Budi"];

function App() {
	let [likes, setLikes] = useState(0);

	const handleClick = () => {
		setLikes((likes += 1));
	};

	return (
		<>
			<Header />
			<ul>
				{students.map((student) => (
					<li key={student}>{student}</li>
				))}
			</ul>
			<button onClick={handleClick}>Like ({likes})</button>
		</>
	);
}

export default App;
